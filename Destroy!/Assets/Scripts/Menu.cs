using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace UtopiaDystopia
{
    public class Menu : MonoBehaviour
    {

        [SerializeField] private AudioSource Bgm;

        // Start is called before the first frame update
        void Start()
        {
            Bgm.Play();
        }

        void StartGame()
        {
            SceneManager.LoadScene("GameStart");
        }
    }
}
